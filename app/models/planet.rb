class Planet < ApplicationRecord
  validates :name, uniqueness: true
end
