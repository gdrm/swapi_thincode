require 'rails_helper'

RSpec.describe PlanetsController, type: :request do
  let(:request_url) { '/planets/create' }
  let(:request_headers) { { CONTENT_TYPE: 'application/json', ACCEPT: 'application/json' } }
  subject do
    get request_url, headers: request_headers
    response
  end

  let(:stubbed_response) do
    {
      "count": 2,
      "results": [
        {
          "name": "Alderaan",
          "climate": "temperate",
          "population": "2000000000",
        },
        {
          "name": "Tatooine",
          "climate": "arid",
          "population": "200000",
        }
      ]
    }
  end

  let!(:stub) do
    stub_request(:get, "https://swapi.co/api/planets").
      to_return({status: 200, body: stubbed_response.to_json})
  end

  describe "GET #create" do
    it "returns http success" do
      is_expected.to have_http_status(:success)
    end

    context "responds with the expected body" do
      before { SwapiClient.class_variable_set(:@@planets, nil) }
      let(:stubbed_response) do
        {
          "count": 1,
          "results": [
            {
              "name": "Alderaan",
              "climate": "temperate",
              "population": "2000000000",
            }
          ]
        }
      end
      let(:expected_body) do
        {
          'name' => 'Alderaan',
          'climate' => 'temperate',
          'population' => 2_000_000_000,
        }
      end

      it { expect(subject.parsed_body).to eq(expected_body) }
    end

    context 'request other planet info when the planet already exists' do
      before do
        SwapiClient.class_variable_set(:@@planets, nil)
        Planet.create(name: 'Alderaan', climate: 'temperate', population: 2_000_000_000)
      end
      let(:expected_body) do
        {
          'name' => 'Tatooine',
          'climate' => 'arid',
          'population' => 200_000,
        }
      end

      it do
        expect(subject.parsed_body).to eq(expected_body)
        expect(stub).to have_been_requested.times 1
      end
    end

    context 'errors when all SWAPI planets have been created' do
      before do
        SwapiClient.class_variable_set(:@@planets, nil)
        Planet.create(name: 'Alderaan', climate: 'temperate', population: 2_000_000_000)
        Planet.create(name: 'Tatooine', climate: 'arid', population: 200_000)
      end

      it do
        expect { subject }.to raise_error(ActiveRecord::RecordInvalid)
        expect(stub).to have_been_requested.times 1
      end
    end
  end
end
